# API

A simple ecommerce API.

## Deployment

1. Install Go.
2. Install the database (sqlite) library [go-sqlite3](https://github.com/mattn/go-sqlite3).
3. Install the JWT library for authentication[jwt-go](https://github.com/dgrijalva/jwt-go).

`go get -u github.com/mattn/go-sqlite3

go get -u github.com/dgrijalva/jwt-go
`

## One line deployment:

`cat database.sql | sqlite3 wingify.db && go build -o win && ./win`

## Documentation:

* All products

GET /v1/products/

Description:
Return all the products in our store. The user needs to be logged in to view the list of products. There is no user specific product listing.

> Note: The request and Response format are in the format of Firefox addon RESTClient, you can just copy paste it to import in your instance.

Request:

`{
    "GET /v1/products": {
        "method": "GET",
        "url": "http://127.0.0.1:9091/v1/products/",
        "body": "",
        "overrideMimeType": false,
        "headers": [
            [
                "Token",
                "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InN1cmFqIiwiZXhwIjoxNDg4NzA1NjI4fQ.mZtfbmwNKLAi1b9iCJ0hSHkLa801YLlVMQBQWvp-J7s"
            ]
        ]
    }
}`

Response:

`[
    {
        "itemName": "the myth of strong leader",
        "itemType": "cart",
        "price": 100,
        "createdBy": "Jake Archibald",
        "photoLink": "amazon"
    },
    {
        "itemName": "business adventures",
        "itemType": "Book",
        "price": 100,
        "createdBy": "Jake Archibald",
        "photoLink": "https://google.com/jakearchibald\n"
    },
    {
        "itemName": "Business business",
        "itemType": "book",
        "price": 200,
        "createdBy": "jake archibald",
        "photoLink": "https://google.com/jakearchibald"
    }
]`

* Search

GET /v1/products?name=

Keywords
	name: the partial name of the product.

Request:

`"GET /v1/search/?name=busin": {
        "method": "GET",
        "url": "http://127.0.0.1:9091/v1/search/?name=business",
        "body": "",
        "overrideMimeType": false,
        "headers": [
            [
                "Content-Type",
                "application/x-www-form-urlencoded"
            ],
            [
                "Token",
                "  eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InN1cmFqIiwiZXhwIjoxNDg4NzA2NzM0fQ.ykbEY2J30i3UQXHs2Q2nvE4TqnQrmQZtD_BbDrASSUk"
            ]
        ]
    }
`
Response:

`[
    {
        "itemName": "business adventures",
        "itemType": "Book",
        "price": 100,
        "createdBy": "Jake Archibald",
        "photoLink": "https://google.com/jakearchibald\n"
    },
    {
        "itemName": "Business business",
        "itemType": "book",
        "price": 200,
        "createdBy": "jake archibald",
        "photoLink": "https://google.com/jakearchibald"
    }
]
`

* Adding a new product

POST /v1/products/ 

Form data:
	product name, product type, createdBy, photoLink, cost.

Request:

`"POST /v1/products/": {
        "method": "POST",
        "url": "http://127.0.0.1:9091/v1/products/",
        "body": "productName=Sherlock Holmes&productType=book&createdBy=Sir Arthur Connan Doyle&photoLink=https://google.com/sirarthur&price=200.20",
        "overrideMimeType": false,
        "headers": [
            [
                "Content-Type",
                "application/x-www-form-urlencoded"
            ],
            [
                "Token",
                "  eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InN1cmFqIiwiZXhwIjoxNDg4NzA2NzM0fQ.ykbEY2J30i3UQXHs2Q2nvE4TqnQrmQZtD_BbDrASSUk"
            ]
        ]
    }
`

Response:

`{
    "messageType": "Information",
    "userMessage": "Item Added",
    "developerMessage": "Item Added",
    "documentationLink": ""
}
`

* Updating product

PUT /v1/products/1234 

Update the product in the database by the name.

Request:

`"PUT /v1/products/": {
        "method": "PUT",
        "url": "http://127.0.0.1:9091/v1/products/",
        "body": "productName=Sherlock Holmes&productType=Novel&createdBy=Sir Arthur Connan Doyle&photoLink=https://google.com/sirarthur&price=200.20",
        "overrideMimeType": false,
        "headers": [
            [
                "Content-Type",
                "application/x-www-form-urlencoded"
            ],
            [
                "Token",
                "  eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InN1cmFqIiwiZXhwIjoxNDg4NzA2NzM0fQ.ykbEY2J30i3UQXHs2Q2nvE4TqnQrmQZtD_BbDrASSUk"
            ]
        ]
    }
 `   
Response:

`{
   "messageType": "Information",
   "userMessage": "Update successful",
   "developerMessage": "Update successful",
   "documentationLink": ""
}`

* User information

GET /v1/purchases/
Description: Purchases of the logged in user.

Request:

`"GET /v1/purchases/": {
        "method": "GET",
        "url": "http://127.0.0.1:9091/v1/purchases/",
        "body": "",
        "overrideMimeType": false,
        "headers": [
            [
                "Token",
                "  eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InN1cmFqIiwiZXhwIjoxNDg4NzA1NjI4fQ.mZtfbmwNKLAi1b9iCJ0hSHkLa801YLlVMQBQWvp-J7s"
            ]
        ]
    }
`
Response:

`  {
        "itemName": "the myth of strong leader",
        "cost": 100.12
  }
`

* User Settings
GET /v1/settings/
Setting of the logged in user.

Request:

 `"GET /v1/purchases/": {
        "method": "GET",
        "url": "http://127.0.0.1:9091/v1/purchases/",
        "body": "",
        "overrideMimeType": false,
        "headers": [
            [
                "Token",
                "  eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InN1cmFqIiwiZXhwIjoxNDg4NzA1NjI4fQ.mZtfbmwNKLAi1b9iCJ0hSHkLa801YLlVMQBQWvp-J7s"
            ]
        ]
    }
`

Response:

`{
    "username": "suraj",
    "name": "Suraj Patil",
    "age": 24,
    "isPrimeEnabled": false
}`

### Reacting to API

Error: When something goes wrong (either client side or server side) it is an error.

200 StatusOK: when everything goes right.
400 StatusBadRequest: If something goes wrong.

`{
   "messageType": "Error",
   "userMessage": "Something went wrong.",
   "developerMessage": "Invalid HTTP method.",
   "documentationLink": ""
}`

Output: The output of the API is in JSON format in which data will be transferred to the client from the server, optional data type filter would be present

HTTP methods:
1. GET: to fetch data
2. PUT: to update data
3. POST: to add data
4. DELETE: to delete data

User Auth
We will be using JSON tokens for user auth, the client is responsible for caching the secret token.

user flow:
`
client ->[username=suraj&password=suraj]  server

client <-[token:]					        server

client ->[header: token GET /v1/products/  ] server

client <-[{JSON data}] 					server
`
