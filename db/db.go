package db

import (
	"log"

	"github.com/thewhitetulip/wingify/types"
)

// DeleteProduct deletes the product passed as the second argument if the username is the seller.
func DeleteProduct(username, productName string) bool {
	err = taskQuery("delete from product where name=?", productName)
	if err != nil {
		return false
	}
	print("Deleting" + productName)
	return true
}

// ValidUser returns bool value true if the username and password match, false otherwise.
func ValidUser(username, password string) bool {
	var passwordFromDB string
	userSQL := "select password from user where username=?"
	log.Println("validating ", username)
	rows := database.query(userSQL, username)

	defer rows.Close()
	if rows.Next() {
		err := rows.Scan(&passwordFromDB)
		if err != nil {
			return false
		}
	}
	if password == passwordFromDB {
		return true
	}
	return false
}

// AddProduct adds the product to the database.
func AddProduct(username, productName, productType, createdBy, photoLink string, cost float64) bool {
	err = taskQuery("insert into product(name, type, created_by, photo_link, insrt_usrnm,insrt_ts,cost) values(?,?,?,?,datetime(), datetime(), ?)", productName, productType, createdBy, photoLink, cost)
	if err != nil {
		return false
	}
	return true
}

// UpdateProduct updates the product in the database.
func UpdateProduct(productName, productType, createdBy, photoLink string) bool {
	err = taskQuery("update product set  type=?, created_by=?, photo_link=?, updt_ts=datetime(), updt_usrnm=? where name=?", productType, createdBy, photoLink, "surajp", productName)
	if err != nil {
		return false
	}
	return true
}

// GetUserSettings returns the settings of the logged in user.
func GetUserSettings(username string) (types.User, error) {
	var user types.User
	rows := database.query("select username, full_name, age from user where username=?", username)

	defer rows.Close()
	if rows.Next() {
		err := rows.Scan(&user.Username, &user.Name, &user.Age)
		if err != nil {
			return types.User{}, err
		}
	}
	return user, nil
}

// GetProductByName returns all products which match this, the search feature.
func GetProductByName(username, productName string) (types.Products, error) {
	var products types.Products
	var product types.Product
	log.Println("searching for product name ", productName)
	sql := "select p.name, p.type, p.created_by, p.photo_link, p.cost from product p where name like '%" + productName + "%'"
	rows := database.query(sql)

	for rows.Next() {
		err := rows.Scan(&product.ItemName, &product.ItemType, &product.CreatedBy, &product.PhotoLink, &product.Price)
		if err != nil {
			return types.Products{}, err
		}

		products = append(products, product)
	}

	return products, nil
}

// GetProducts returns all products available for the loggedin user.
func GetProducts(username string) (types.Products, error) {
	var products types.Products
	var product types.Product
	sql := "select p.name, p.type, p.created_by, p.photo_link, p.cost from product p"
	rows := database.query(sql, username)

	for rows.Next() {
		err := rows.Scan(&product.ItemName, &product.ItemType, &product.CreatedBy, &product.PhotoLink, &product.Price)
		log.Println(err)
		if err != nil {
			return types.Products{}, err
		}

		products = append(products, product)
	}

	return products, nil
}

// GetPurchases returns the purchases of the loggedin user.
func GetPurchases(username string) (types.Purchases, error) {
	var purchases types.Purchases
	var purchase types.Purchase
	sql := "select pro.name, pur.price from purchases pur, user u, product pro where pur.user_id = u.id and pro.id = pur.product_id and u.username=?"
	rows := database.query(sql, username)
	for rows.Next() {
		err = rows.Scan(&purchase.ItemName, &purchase.Cost)
		if err != nil {
			return types.Purchases{}, err
		}
		purchases = append(purchases, purchase)
	}

	return purchases, nil
}
