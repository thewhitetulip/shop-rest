package db

import (
	"database/sql"
	"log"

	_ "github.com/mattn/go-sqlite3"
)

var database Database
var err error

// Database encapsulates the database object.
type Database struct {
	db *sql.DB
}

// begin will start a transaction on the db object
func (db Database) begin() (tx *sql.Tx) {
	tx, err := db.db.Begin()
	if err != nil {
		log.Println(err)
		return nil
	}
	return tx
}

func (db Database) prepare(q string) (stmt *sql.Stmt) {
	stmt, err := db.db.Prepare(q)
	if err != nil {
		log.Println(err)
		return nil
	}
	return stmt
}

func (db Database) query(q string, args ...interface{}) (rows *sql.Rows) {
	rows, err := db.db.Query(q, args...)
	if err != nil {
		log.Println(err)
		return nil
	}
	return rows
}

func init() {
	database.db, err = sql.Open("sqlite3", "./wingify.db")
	if err != nil {
		log.Println("Error opening wingify.db")
	}
}

func taskQuery(sql string, args ...interface{}) error {
	log.Println("inside task query")
	SQL := database.prepare(sql)
	tx := database.begin()
	_, err = tx.Stmt(SQL).Exec(args...)

	if err != nil {
		log.Println("taskQuery: ", err)
		tx.Rollback()
	} else {
		err = tx.Commit()
		if err != nil {
			log.Println(err)
			return err
		}
		log.Println("commit successful")
	}
	return err
}
func Close() {
	database.db.Close()
}
