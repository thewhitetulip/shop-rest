package main

import (
	"log"
	"net/http"

	"github.com/thewhitetulip/wingify/views"
)

func main() {
	http.HandleFunc("/v1/token/", views.GetTokenHandler)

	http.HandleFunc("/v1/search/", views.SearchHandler)
	http.HandleFunc("/v1/products/", views.ProductsHandler)
	http.HandleFunc("/v1/purchases/", views.PurchasesHandler)
	http.HandleFunc("/v1/settings/", views.UserSettingsHandler)

	log.Println("Starting server on http://127.0.0.1:9091")
	log.Fatal(http.ListenAndServe(":9091", nil))

}
