package types

/*
Package types is used to store the custom defined structs.
*/

// Message struct represents the JSON document which the API sends when something wrong will happen.
// Type: If this is an error message, "Error" or "Message" can be the two values.
// UserMessage: This is the string which the developer can show to the user.
// DeveloperMessage: This is the technical message.
// DocumentationLink: If the message type is error, this will be the link to corresponding documentation.
type Message struct {
	Type              string `json:"messageType"`
	UserMessage       string `json:"userMessage"`
	DeveloperMessage  string `json:"developerMessage"`
	DocumentationLink string `json:"documentationLink"`
}

// Product struct represents the JSON document which represents a product the user will see and buy.
// ID: the Database ID regarding the product.
// ItemName: the name of the product.
// ItemType: the type of the product (furniture, book etc)
// CreatedBy: author of the book or manufacturer of the item.
// PhotoLink: link to the photo.
// Price: price of the product
// Sellers: an array of Sellers data type.
type Product struct {
	ID        float32  `json:"id,omitempty"`
	ItemName  string   `json:"itemName"`
	ItemType  string   `json:"itemType"`
	Price     float32  `json:"price"`
	CreatedBy string   `json:"createdBy"`
	PhotoLink string   `json:"photoLink"`
	Sellers   []Seller `json:"sellers,omitempty"`
}

type Products []Product

// Seller
type Seller struct {
	Name    string  `json:"name"`
	Address string  `json:"address"`
	Price   float32 `json:"price"`
}

// Review type refers to the review that a customer writes against a product.
// ProductID: ID of the product against which this review was written.
// Type: Type of the review.
// Rating: Rating given by the customer, if this rating > 3 then the `Type` is Positive, else negative.
// Username: The username of the user which gave the rating.
// ReviewContent: The actual content of the review.
type Review struct {
	ProductID float32 `json:"productID"`
	Type      string  `json:"type"`
	Rating    int     `json:"rating"`
	Username  string  `json:"username"`
	Content   string  `json:"content"`
}

// User structure represents a user.
// Username: the username of the user, guaranteed to be unique.
// Name: the name of the user.
// Age: the age of the user.
// Password: hash of the password.
// IsPrimeEnabled: boolean, true if prime is enabled, false otherwise.
type User struct {
	Username       string `json:"username"`
	Name           string `json:"name"`
	Age            int    `json:"age"`
	Password       string `json:"password,omitempty"`
	IsPrimeEnabled bool   `json:"isPrimeEnabled"`
}

// Purchase struct represents one purchase.
// ItemName: the name of the item which is purchased.
// Cost: the cost of the item when purchased.
type Purchase struct {
	ItemName string  `json:"itemName"`
	Cost     float32 `json:"cost"`
}

type Purchases []Purchase
