package views

// views package holds the function definition of the views which will be used to handle the URLs in the API.

import (
	"encoding/json"
	"log"
	"net/http"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/thewhitetulip/wingify/db"
	"github.com/thewhitetulip/wingify/types"
)

type MyCustomClaims struct {
	Username string `json:"username"`
	jwt.StandardClaims
}

var mySigningKey = []byte("secret")

// ReturnMessageJSON is a wrapper which will send JSON document of type Message, it takes the following arguments
// messageType: Error or Information, denotes if the message is just FYI or an error
// userMessage: Message in terms of user
// devMessage: Message in terms of Developer
func ReturnMessageJSON(w http.ResponseWriter, messageType, userMessage, devMessage string) {
	message := types.Message{Type: messageType, UserMessage: userMessage, DeveloperMessage: devMessage}
	if messageType == "Information" {
		w.WriteHeader(http.StatusOK)
	} else {
		w.WriteHeader(http.StatusBadRequest)
	}
	err = json.NewEncoder(w).Encode(message)
	if err != nil {
		panic(err)
	}
	return
}

//GetTokenHandler will get a token for the username and password
func GetTokenHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method != "POST" {
		ReturnMessageJSON(w, "Error", "Page not available", "GetTokenHandler only accepts a POST")
		return
	}

	r.ParseForm()
	username := r.Form.Get("username")
	password := r.Form.Get("password")
	log.Println(r.Form)
	if username == "" || password == "" {
		ReturnMessageJSON(w, "Error", "Invalid Username/Password", "Invalid Username or password in GetTokenHandler")
		return
	}
	if db.ValidUser(username, password) {
		// Create the Claim which expires after EXPIRATION_HOURS hrs, default is 5.
		claims := MyCustomClaims{
			username,
			jwt.StandardClaims{
				ExpiresAt: time.Now().Add(time.Hour * 5).Unix(),
			},
		}

		token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

		/* Sign the token with our secret */
		tokenString, err := token.SignedString(mySigningKey)
		if err != nil {
			log.Println("Something went wrong with signing token")
			ReturnMessageJSON(w, "Error", "Authentication Failed", "Authentication Failed")

			return
		}

		/* Finally, write the token to the browser window */
		w.Write([]byte(tokenString))
	} else {
		ReturnMessageJSON(w, "Error", "Authentication Failed", "Authentication Failed")
	}
}

//ValidateToken will validate the token
func ValidateToken(myToken string) (bool, string) {
	token, err := jwt.ParseWithClaims(myToken, &MyCustomClaims{}, func(token *jwt.Token) (interface{}, error) {
		return []byte(mySigningKey), nil
	})

	if err != nil {
		log.Println("Invalid token.", token)
		return false, ""
	}

	claims := token.Claims.(*MyCustomClaims)
	return token.Valid, claims.Username
}
