package views

import (
	"encoding/json"
	"log"
	"net/http"
	"strconv"

	"github.com/thewhitetulip/wingify/db"
	"github.com/thewhitetulip/wingify/types"
)

var message types.Message
var err error

// SearchHandler handles the search page.
func SearchHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	if r.Method != "GET" {
		ReturnMessageJSON(w, "Error", "Unable to open Page", "Invalid HTTP req")
		return
	}

	r.ParseForm()
	ok, username := CheckTokenGetUsername(r)
	if !ok {
		ReturnMessageJSON(w, "Error", "You are not authorized to do this, login & try again", "Invalid token, UserSettingsHandler")
		return
	}

	productName := r.Form.Get("name")
	products, err := db.GetProductByName(username, productName)
	if err != nil {
		ReturnMessageJSON(w, "Error", "Can't get product", "Buffer overflow")
		return
	}
	w.WriteHeader(http.StatusOK)
	err = json.NewEncoder(w).Encode(products)
	if err != nil {
		panic(err)
	}
	return
}

// ProductsHandler handles the products page, returns all the products.
func ProductsHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	ok, username := CheckTokenGetUsername(r)
	if !ok {
		ReturnMessageJSON(w, "Error", "You are not authorized to do this, login & try again", "Invalid token, UserSettingsHandler")
		return
	}

	if r.Method == "GET" {
		var products types.Products
		products, err = db.GetProducts(username)

		if err != nil {
			ReturnMessageJSON(w, "Error", "Could't get product", "error")
			log.Println(err)
			return
		}

		w.WriteHeader(http.StatusOK)

		err = json.NewEncoder(w).Encode(products)

		if err != nil {
			panic(err)
		}
		return

	} else if r.Method == "POST" {
		r.ParseForm()
		productName := r.Form.Get("productName")
		productType := r.Form.Get("productType")
		createdBy := r.Form.Get("createdBy")
		photoLink := r.Form.Get("photoLink")
		price := r.Form.Get("price")
		cost, err := strconv.ParseFloat(price, 32)

		if err != nil {
			ReturnMessageJSON(w, "Error", "Invalid Cost", "error")
			log.Println(err)
			return
		}

		ok = db.AddProduct(username, productName, productType, createdBy, photoLink, cost)
		ReturnMessageJSON(w, "Information", "Item Added", "Item Added")
	} else if r.Method == "DELETE" {
		slug := r.URL.Path[len("/v1/products/"):]
		ok := db.DeleteProduct(username, slug)

		if !ok {
			ReturnMessageJSON(w, "Error", "Unable to delete", "Something went wrong while deleting product")
			return
		}
		ReturnMessageJSON(w, "Information", "Product Deleted", "Product Deleted")

	} else if r.Method == "PUT" {
		r.ParseForm()
		productName := r.Form.Get("productName")
		productType := r.Form.Get("productType")
		createdBy := r.Form.Get("createdBy")
		photoLink := r.Form.Get("photoLink")

		ok = db.UpdateProduct(productName, productType, createdBy, photoLink)

		if !ok {
			ReturnMessageJSON(w, "Error", "Unable to update, try again please", "something went wrong in updating the product")
			return
		}
		ReturnMessageJSON(w, "Information", "Update successful", "Update successful")
	} else {
		ReturnMessageJSON(w, "Error", "Can't access the page", "Invalid method for Products")
	}
}

// PurchasesHandler returns the list of purchases of the logged in user.
func PurchasesHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	ok, username := CheckTokenGetUsername(r)

	if !ok {
		ReturnMessageJSON(w, "Error", "You are not authorized to do this, login & try again", "Invalid token, UserSettingsHandler")
		return
	}

	if r.Method != "GET" {
		ReturnMessageJSON(w, "Error", "Invalid Request", "UsersHandler supports only GET request")
		return
	}
	purchases, err := db.GetPurchases(username)

	if err != nil {
		ReturnMessageJSON(w, "Error", "Error happened", "Error")
		log.Println(err)
		return
	}

	w.WriteHeader(http.StatusOK)
	err = json.NewEncoder(w).Encode(purchases)

	if err != nil {
		panic(err)
	}
	return
}

// CheckTokenGetUsername gets the HTTP request as the argument and returns if the token is valid
// which is passed as a header of the name Token and returns the username of the logged in user.
func CheckTokenGetUsername(r *http.Request) (bool, string) {
	token := r.Header["Token"][0]
	ok, username := ValidateToken(token)
	return ok, username
}

// UserSettingsHandler handles the /v1/settings handler and if the user is logged in, returns the JSON document of the settings of the user.
func UserSettingsHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	ok, username := CheckTokenGetUsername(r)
	if !ok {
		ReturnMessageJSON(w, "Error", "You are not authorized to do this, login & try again", "Invalid token, UserSettingsHandler")
		return
	}

	if r.Method != "GET" {
		ReturnMessageJSON(w, "Error", "Invalid Request", "UsersHandler supports only GET request")
		return
	}

	userSettings, err := db.GetUserSettings(username)

	if err != nil {
		ReturnMessageJSON(w, "Error", "Error happened", "err")
		log.Println(err)
		return
	}

	w.WriteHeader(http.StatusOK)
	err = json.NewEncoder(w).Encode(userSettings)

	if err != nil {
		panic(err)
	}
	return
}
