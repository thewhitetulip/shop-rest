package views

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"log"
)

var token string

// GetToken fetches a token for our server.
func GetToken() string {
	ts := httptest.NewServer(http.HandlerFunc(SearchHandler))
	defer ts.Close()

	req, err := http.NewRequest("POST", "/v1/token/", nil)

	if err != nil {
		log.Println(err)
	}

	req.Form = make(map[string][]string, 0)
	req.Form.Add("username", "suraj")
	req.Form.Add("password", "suraj")

	rr := httptest.NewRecorder()
	GetTokenHandler(rr, req)

	if rr.Code != http.StatusOK {
		log.Println("Error getting token")
	}
	return rr.Body.String()

}

// TestWrongMethodSearchHandler tests the SearchHandler with the wrong http method,
// except for GET, it should return an error to the client.
func TestWrongMethodSearchHandler(t *testing.T) {

	ts := httptest.NewServer(http.HandlerFunc(SearchHandler))

	token = GetToken()
	testCases := []struct {
		method     string
		wantStatus int
	}{
		{
			"GET",
			http.StatusOK,
		}, {
			"PUT",
			http.StatusBadRequest,
		}, {
			"POST",
			http.StatusBadRequest,
		}, {
			"PATCH",
			http.StatusBadRequest,
		},
	}

	for _, tc := range testCases {

		req, err := http.NewRequest(tc.method, ts.URL, nil)
		req.Form = make(map[string][]string, 0)
		req.Form.Add("name", "business")
		if err != nil {
			t.Fatal(err)
		}
		req.Header.Set("token", token)

		rr := httptest.NewRecorder()
		SearchHandler(rr, req)

		if status := rr.Code; status != tc.wantStatus {
			t.Errorf("handler returned wrong status code: got %v want %v", status, http.StatusOK)

		}
	}
}

// TestSearchHandler checks the response of the SearchHandler, if the token is invalid, it throws an error
// if the token is valid, then we aren't really checking the output of the database.
func TestSearchHandler(t *testing.T) {
	token = GetToken()
	testCases := []struct {
		token     string
		wantCode  int
		wantBody  string
		checkBody bool
	}{{
		token,
		http.StatusOK,
		"",
		false,
	}, {
		"thisandthat",
		http.StatusBadRequest,
		`{"messageType":"Error","userMessage":"You are not authorized to do this, login \u0026 try again","developerMessage":"Invalid token, UserSettingsHandler","documentationLink":""}`,
		true,
	}}

	ts := httptest.NewServer(http.HandlerFunc(SearchHandler))
	defer ts.Close()

	for _, tc := range testCases {

		req, err := http.NewRequest("GET", ts.URL, nil)
		req.Form = make(map[string][]string, 0)
		req.Form.Add("name", "business")
		if err != nil {
			t.Fatal(err)
		}
		req.Header.Set("token", tc.token)

		rr := httptest.NewRecorder()
		SearchHandler(rr, req)

		if status := rr.Code; status != tc.wantCode {
			t.Errorf("handler returned wrong status code: got %v want %v", status, tc.wantCode)
			if tc.checkBody && rr.Body.String() != tc.wantBody {
				t.Errorf("handler got unexpected body: got %v want %v", rr.Body.String(), tc.wantBody)
			}
		}
	}
}

// TestAddHandler tests the POST request to /v1/products/, it should return the Message JSON if success and error JSON if error happens.
func TestAddHandler(t *testing.T) {
	token = GetToken()
	expected := `{"messageType":"Information","userMessage":"Item Added","developerMessage":"Item Added","documentationLink":""}`

	ts := httptest.NewServer(http.HandlerFunc(ProductsHandler))
	defer ts.Close()

	req, err := http.NewRequest("POST", ts.URL, nil)
	req.Form = make(map[string][]string, 0)

	req.Form.Add("productName", "This is the Batman")
	req.Form.Add("productType", "Batmobile")
	req.Form.Add("createdBy", "DC comics")
	req.Form.Add("photoLink", "https://batman.com")
	req.Form.Add("price", "200.12")

	if err != nil {
		t.Fatal(err)
	}

	req.Header.Set("token", token)

	rr := httptest.NewRecorder()
	ProductsHandler(rr, req)

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code, got %v expected %v", status, http.StatusOK)
		if rr.Body.String() != expected {
			t.Errorf("handler got unexpected body: got %v expected %v", rr.Body.String(), expected)
		}
	}
}

// TestDeleteProductHandler will return an error if the request method is anything other than a GET.
func TestDeleteProductHandler(t *testing.T) {
	token = GetToken()
	testCases := []struct {
		token     string
		wantCode  int
		wantBody  string
		checkBody bool
	}{{
		"thisandthat",
		http.StatusBadRequest,
		`{"messageType":"Error","userMessage":"You are not authorized to do this, login \u0026 try again","developerMessage":"Invalid token, UserSettingsHandler","documentationLink":""}`,
		true,
	}, {
		token,
		http.StatusOK,
		"",
		false,
	}}

	ts := httptest.NewServer(http.HandlerFunc(SearchHandler))
	defer ts.Close()

	for _, tc := range testCases {
		// we will delete the product This is the Batman which we added in the above test case, it should not matter because we are in a mock db.

		req, err := http.NewRequest("DELETE", "/v1/products/This is the Batman", nil)
		if err != nil {
			t.Fatal(err)
		}
		req.Header.Set("token", tc.token)

		rr := httptest.NewRecorder()
		ProductsHandler(rr, req)

		if status := rr.Code; status != tc.wantCode {
			t.Errorf("handler returned wrong status code: got %v want %v", status, tc.wantCode)
			if tc.checkBody && rr.Body.String() != tc.wantBody {
				t.Errorf("handler got unexpected body: got %v want %v", rr.Body.String(), tc.wantBody)
			}
		}
	}
}
